class App {
  constructor() {
    this.clearButton = document.getElementById("clear-btn");
    this.loadButton = document.getElementById("load-btn");
    this.carContainerElement = document.getElementById("cars-container");
  }

  async init(params) {
    // await this.load();

    // // Register click listener
    // this.clearButton.onclick = this.clear;
    // this.loadButton.onclick = this.run;
    const { driver, date, waktu, kapasitas: cap = 0 } = params;
    const filterCars = ({ available, capacity, availableAt }, item) => {
      if (available==true && availableAt.substring(0, 12) < `${date}T${waktu}` && capacity >= cap) {
        return item;
      }
    };
    console.log('params', params)
    const cars = await Binar.listCars(filterCars);
    Car.init(cars);

  }

  run = () => {
    Car.list.forEach((car) => {
      const node = document.createElement("div");
      node.innerHTML = car.render();
      node.classList.add("col-md-4");
      this.carContainerElement.appendChild(node);
    });
  };


  // async load() {
  //   const cars = await Binar.listCars();
  //   Car.init(cars);
  // }

  clear = () => {
    let child = this.carContainerElement.firstElementChild;

    while (child) {
      child.remove();
      child = this.carContainerElement.firstElementChild;
    }
  };
}