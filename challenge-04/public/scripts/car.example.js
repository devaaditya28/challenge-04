class Car {
  static list = [];

  static init(cars) {
    this.list = cars.map((i) => new this(i));
  }

  constructor({
    id,
    plate,
    manufacture,
    model,
    image,
    rentPerDay,
    capacity,
    description,
    transmission,
    available,
    type,
    year,
    options,
    specs,
    availableAt,
  }) {
    this.id = id;
    this.plate = plate;
    this.manufacture = manufacture;
    this.model = model;
    this.image = image;
    this.rentPerDay = rentPerDay;
    this.capacity = capacity;
    this.description = description;
    this.transmission = transmission;
    this.available = available;
    this.type = type;
    this.year = year;
    this.options = options;
    this.specs = specs;
    this.availableAt = availableAt;
  }

  render() {
    return `
        
        <div class="card bg-white shadow-sm rounded-3">
            <div class="card-body">
                <div class="image" >
                    <img class="card-img-top" src="${this.image}" alt="cars" >
                </div>

                <div class="mobilName">
                    <p>${this.manufacture}</p>
                </div>

                <div class="harga">
                    <p>${this.rentPerDay}</p>
                </div>
                <div class="description">
                    <p>${this.description} </p>
                </div>

                <div class="spesifikasi">
                    <div class="kapasitas mt-3 mb-2">
                        <div class="row">
                            <div class="col-lg-2 col-md-2">
                            <i class="fa-solid fa-user-group" alt="kapasitas"></i> 
                            </div>
                            <div class="col-lg-8 col-md-8 text-left">
                                <p>${this.capacity}</p>
                            </div>
                        </div> 
                    </div>

                    <div class="setting mb-2">
                        <div class="row">
                            <div class="col-lg-2 col-md-2">
                            <i class="fa-solid fa-gear" alt="kapasitas"></i>
                            </div>
                            <div class="col-lg-8 col-md-8 text-left">
                                <p>${this.transmission}</p>
                            </div>
                        </div> 
                    </div>

                    <div class="year mb-2">
                        <div class="row">
                            <div class="col-lg-2 col-md-2">
                            <i class="fa-solid fa-calendar" alt="kapasitas"></i>
                            </div>
                            <div class="col-lg-8 col-md-8 text-left">
                                <p>${this.year}</p>
                            </div>
                        </div> 
                    </div>
                </div>

            <div class="pilihMobil mb-2">
                <button class="btn btn-lg text-white" type="submit" style="width: 100%; background-color: #5CB85F;">Pilih Mobil</button>
            </div>
          </div>
      </div>

    `;
  }
}